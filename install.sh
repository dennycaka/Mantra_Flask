#!/bin/bash

if ! [ -x "$(command -v wkhtmltopdf)" ]; then
        sudo apt-get -y install wkhtmltopdf
else
        echo "[+] wkhtmltopdf already installed. Skipping to the next installation ..." >&2
fi

if ! [ -x "$(command -v gem)" ]; then
	echo '[x] Error: gem is not installed. Install ruby first!' >&2
	exit 1
else
	if ! [ -x "$(command -v wpscan)" ]; then
		gem install wpscan
	else
		echo "[+] WPScan already installed. Skipping to the next installation ..." >&2
	fi
fi

pip3 install -r requirements.txt

read -n1 -p "Are you sure want to create new database? [y/n] " confirm

echo -e "\n"

if [[ $confirm == "y" || $confirm == "y" ]]; then
	python3 db_create.py
else
	echo "Done!"
fi

chmod +x run.py && chmod +x mantra/api/mantra_scan_api.py

cd mantra/api/plugins

echo "Cloning JoomScan ..."
git clone https://github.com/rezasp/joomscan.git
echo -e "Done ...\n"

echo "Cloning SQLMap ..."
git clone https://github.com/sqlmapproject/sqlmap.git
echo "Done ..."
