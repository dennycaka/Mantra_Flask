# File to run the Mantra Application
from mantra import app

# Setting host and port
host = '127.0.0.1'
port = '5000'

if __name__ == '__main__':
    app.run(host=host, port=port, debug=True)