# Utilities for Mantra auth module

# ==================== #
#     Requirements     #
# ==================== #

# Import OS module
import os
# Import secrets module
import secrets

# Import Image adjuster from pillow
from PIL import Image, ImageOps

# Import object from Mantra module
from mantra import app
# Import models required in the module
from mantra.models import User

# ==================== #


# ============================= #
#     Save Picture Function     #
# ============================= #
def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(app.root_path, 'static/dist/img/', picture_fn)
    output_size = (215, 215)
    img = Image.open(form_picture)
    thumbs = ImageOps.fit(img, output_size, Image.ANTIALIAS)
    thumbs.save(picture_path)
    return picture_fn
