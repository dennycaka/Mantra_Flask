# Forms for Mantra main module

# ====================== #
#      Requirements      #
# ====================== #

# Import datetime module
from datetime import datetime

# Import FlaskForm module
from flask_wtf import FlaskForm
# Import form field elements
from wtforms import SelectField, SubmitField

# Import models required in the module
from mantra.models import StatisticYear

# ====================== #


# =========================== #
#     Statistic Year Form     #
# =========================== #
class StatisticYearForm(FlaskForm):
    def choice_year():
        available_years = StatisticYear.query.all()
        year_list = [(int(i.year), i.year) for i in available_years]
        return year_list
    def current_year():
        this_year = datetime.utcnow().year
        return int(this_year)
    year = SelectField('Select year', coerce=int, choices=(choice_year()),
                       default=current_year())
