# mantra/test_view_statistic.py

# Import os
import os
# Import unittest as base for unit testing
import unittest
# Import elements from Mantra Application
from mantra import app, basedir, db


TEST_DB = 'test.db'


class BasicTests(unittest.TestCase):

    ############################
    #### setup and teardown ####
    ############################

    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, TEST_DB)
        with app.app_context():
            db.create_all()
        self.app = app.test_client()

    # executed after each test
    def tearDown(self):
        db.drop_all()

    ########################
    #### helper methods ####
    ########################

    def register(self):
        return self.app.post(
            '/auth/register',
            data=dict(
                username='user1',
                name='user1',
                email='user1@test.com',
                password='test123456',
                confirm_password='test123456'
            ),
            follow_redirects=True
        )

    def login(self):
        return self.app.post(
            '/auth/login',
            data=dict(
                username='user1',
                password='test123456',
                remember=True
            ),
            follow_redirects=True
        )

###############
#### tests ####
###############

    # Test valid view statistic
    def test_1_valid_view_statistic(self):
        response_register = self.register()
        self.assertEqual(response_register.status_code, 200)
        response_login = self.login()
        self.assertEqual(response_login.status_code, 200)
        print('Test valid view statistic')
        response = self.app.get('/home', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Dashboard', response.data)

    # Test invalid view statistic
    def test_2_invalid_view_statistic(self):
        print('Test invalid view statistic')
        response = self.app.get('/home', follow_redirects=True)
        self.assertEqual(response.status_code, 200)
        self.assertIn(b'Sign in to Mantra', response.data)

if __name__ == "__main__":
    unittest.main()