# mantra/test_edit_profile.py

# Import os
import os
# Import unittest as base for unit testing
import unittest
# Import elements from Mantra Application
from mantra import app, basedir, db


TEST_DB = 'test.db'


class BasicTests(unittest.TestCase):

    ############################
    #### setup and teardown ####
    ############################

    # executed prior to each test
    def setUp(self):
        app.config['TESTING'] = True
        app.config['WTF_CSRF_ENABLED'] = False
        app.config['DEBUG'] = False
        app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///' + os.path.join(basedir, TEST_DB)
        with app.app_context():
            db.create_all()
        self.app = app.test_client()

    # executed after each test
    def tearDown(self):
        db.drop_all()

    ########################
    #### helper methods ####
    ########################

    def register(self):
        return self.app.post(
            '/auth/register',
            data=dict(
                username='test',
                name='test',
                email='test@test.com',
                password='test123456',
                confirm_password='test123456'
            ),
            follow_redirects=True
        )

    def login(self):
        return self.app.post(
            '/auth/login',
            data=dict(
                username='test',
                password='test123456',
                remember=True
            ),
            follow_redirects=True
        )

    def edit_profile(self, username, name, email, new_password, picture):
        return self.app.post(
            '/auth/profile',
            data=dict(
                username=username,
                name=name,
                email=email,
                new_password=new_password,
                picture=picture
            ),
            follow_redirects=True
        )

###############
#### tests ####
###############

    # Test valid edit profile
    def test_1_valid_edit_profile(self):
        response_register = self.register()
        self.assertEqual(response_register.status_code, 200)
        response_login = self.login()
        self.assertEqual(response_login.status_code, 200)
        print('Test valid edit profile')
        response_login = self.edit_profile(
            'userTest', 'nameChanged', 'userTest@test.com', '', 'default.png')
        self.assertEqual(response_login.status_code, 200)
        self.assertIn(b'Profile Updated!', response_login.data)

    # Test invalid edit profile
    def test_2_invalid_edit_profile(self):
        response_register = self.register()
        self.assertEqual(response_register.status_code, 200)
        response_login = self.login()
        self.assertEqual(response_login.status_code, 200)
        print('Test invalid edit profile')
        response_login = self.edit_profile(
            'userTest', 'nameChanged', 'invalidEmail', '', 'default.png')
        self.assertEqual(response_login.status_code, 200)
        self.assertIn(b'Invalid email address.', response_login.data)

if __name__ == "__main__":
    unittest.main()