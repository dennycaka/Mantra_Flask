#!/usr/bin/python3

import configparser

class ConfigParser(object):
    def __new__(self):
        config = configparser.ConfigParser()
        config.read('config.ini')
        return config