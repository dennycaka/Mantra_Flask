#!/usr/bin/python3

from sqlalchemy import create_engine

class DBConnect(object):
    def __new__(self):
        return create_engine('sqlite:///../../mantra/mantra.db', connect_args={'check_same_thread': False})