#!/usr/bin/python3

from threading import Thread
# from checker import Checker
from mode.url_validator import Validator
from core.scanner import Scanner
from db.db import DBConnect
import argparse
import sys
import random
import string
from hashlib import md5

# url = 'http://127.0.0.1:8030/'
# url = 'http://www.cedit.biz/'
# url = 'https://www.mymcpl.org/'
# url = 'http://103.200.7.150:8002/detail.php?id=13'
# url = 'http://testphp.vulnweb.com/artists.php?artist=1'

db_connect = DBConnect()


parser = argparse.ArgumentParser()
parser.add_argument('--id', help='Enter id')
parser.add_argument('--title', help='Enter title')
parser.add_argument('--url', help='Enter URL')
parser.add_argument('--description', help='Enter description', nargs='+')
parser.add_argument('--user_token', help='Enter user token')
parser.add_argument('--platform_id', help='Enter platform id')
parser.add_argument('--platform_type', help='Enter platform type')
args = parser.parse_args()
# args = ' '.join(args.string)

print(args.url)
print(args.title)
print(args.description)

if args.url is not None:
    # scan_id = md5(''.join(random.choices(string.ascii_letters, k=32))).hexdigest()
    scan_id = args.id
    print(scan_id)
    scan_title = args.title
    scan_url = Validator(args.url).validate_url()
    scan_description = args.description
    scan_user_token = args.user_token
    platform_id = args.platform_id
    platform_type = args.platform_type
    # url = Validator("http://www.pqs.ulg.ac.be").validate_url()

    if scan_url:
        print(scan_url)
    else:
        print("Error!")

    # def __init__(self, scan_id, scan_title, scan_url, scan_description, platform_type):

    scan = Scanner(scan_id, scan_title, scan_url, scan_description, platform_type, scan_user_token, platform_id)
    Thread(target=scan.scan).start()