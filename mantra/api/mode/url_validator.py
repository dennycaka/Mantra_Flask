#!/usr/bin/python3

import string

class Validator:
    def __init__(self, url):
        self.url = url

    def validate_url(self):
        url = self.url.lower()
        valid_characters = ['.', '/', ':', '?', '=', '#', '-']
        if not url.endswith("/"):
            url += "/"
        for i in url:
            if not i.islower() and not i.isdigit() and i not in valid_characters:
                return False
        return url