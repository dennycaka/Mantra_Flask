#!/usr/bin/python3

import socket
from subprocess import check_output, CalledProcessError, Popen, STDOUT, PIPE
import configparser
import os

class SQLMapService: 
    def __init__(self):
        self.config = configparser.ConfigParser()
        self.full_path = os.path.dirname(os.path.abspath(__file__))
        self.root_path = os.path.join(self.full_path, '../')
        self.config.read(os.path.join(self.root_path, 'config.ini'))

    def start_sqlmap(self):
        cmd = self.config['runner']['sqlmap'] % (self.config['service']['sqlmap_port'])
        cmd = cmd.split()
        print(cmd)
        # print(cmd)
        try:
            Popen(cmd, stdout=PIPE, stderr=STDOUT)
        except Exception as e:
            print("Error: %s" % (e))

    def service_checker(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex(('127.0.0.1',8778))
        return result