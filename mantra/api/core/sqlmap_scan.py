#!/usr/bin/python3

import socket
from subprocess import check_output, CalledProcessError, Popen, STDOUT, PIPE
import configparser
import os

class SQLMapScan: 
    def __init__(self, url):
        self.url = url
        self.sqlmap_api_url = 'http://127.0.0.1:8778'
        self.headers = {"Content-Type":"application/json"}
        self.config = configparser.ConfigParser()
        self.full_path = os.path.dirname(os.path.abspath(__file__))
        self.root_path = os.path.join(self.full_path, '../')
        self.config.read(os.path.join(self.root_path, 'config.ini'))

    def start_sqlmap(self):
        cmd = self.config['runner']['sqlmap'] % (self.config['service']['sqlmap_port'])
        cmd = cmd.split()
        # print(cmd)
        try:
            Popen(cmd, stdout=PIPE, stderr=STDOUT)
        except Exception as e:
            print("Error: %s" % (e))

    def service_checker(self):
        sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        result = sock.connect_ex(('127.0.0.1',8778))
        return result

    # def start(self):
    #     api_url = requests.get('%s/task/new' % (self.sqlmap_api_url), headers=self.headers).text
    #     return json.loads(api_url)['taskid']

    # def start_sqli_process(self, url):
    #     payload = {'url': url}
    #     print(json.dumps(payload))
    #     url = '%s/scan/%s/start' % (self.sqlmap_api_url, self.start())
    #     api_url = requests.post(url, data=json.dumps(payload), headers=self.headers)
    #     print(url)
    #     print(api_url.text)