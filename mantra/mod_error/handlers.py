# Error handler for Mantra Application

# ==================== #
#     Requirements     #
# ==================== #

# Import dependencies from flask module
from flask import Blueprint, render_template

# ==================== #


# Define the blueprint: 'errors'
mod_errors = Blueprint('errors', __name__)


# Set the error handlers

# ========================= #
#     Error 403 Handler     #
# ========================= #
@mod_errors.app_errorhandler(403)
def error_403(error):
    return render_template('errors/403.html', title='403'), 403


# ========================= #
#     Error 404 Handler     #
# ========================= #
@mod_errors.app_errorhandler(404)
def error_404(error):
    return render_template('errors/404.html', title='404'), 404


# ========================= #
#     Error 500 Handler     #
# ========================= #
@mod_errors.app_errorhandler(500)
def error_500(error):
    return render_template('errors/500.html', title='500'), 500
